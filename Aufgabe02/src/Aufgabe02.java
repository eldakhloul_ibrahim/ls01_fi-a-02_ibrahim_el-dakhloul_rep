
public class Aufgabe02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        System.out.printf("%s%5s%23s%4s\r","0!","=","=","1");

        System.out.printf("%s%7s%21s%4s\r","1!","= 1","=","1");

        System.out.printf("%s%11s%17s%4s\r","2!","= 1 * 2","=","2");

        System.out.printf("%s%15s%13s%4s\r","3!","= 1 * 2 * 3","=","6");

        System.out.printf("%s%19s%9s%4s\r","4!","= 1 * 2 * 3 * 4","=","24");

        System.out.printf("%s%23s%5s%4s\r","5!","= 1 * 2 * 3 * 4 * 5","=","120");
	}

}
